import { update as updateSnake, draw as drawSnake, SNAKE_SPEED, getSnakeHead, snakeInterserction, snakeSpeedInscrease} from "./snake.js";
import { update as updateFood, draw as drawFood} from "./food.js";
import { outsideGrid } from "./grid.js";

let lastRenderTime = 0;
let gameOver = false
const gameBoard = document.getElementById('game-board')

function main (currentTime) {
    if (gameOver) {
        return alert('You lose')
    }
    window.requestAnimationFrame(main);
    const secondsSinceLastRender = (currentTime - lastRenderTime) / 1000
    let newspeed = SNAKE_SPEED + snakeSpeedInscrease();
    if (secondsSinceLastRender < 1 / newspeed) return
    lastRenderTime = currentTime;

    update();
    draw();
}

window.requestAnimationFrame(main);

function update() {
    updateSnake()
    updateFood()
    checkDeath()
    // updateWall()
}

function draw() {
    gameBoard.innerHTML = ""
    drawSnake(gameBoard);
    drawFood(gameBoard);
    // drawWall(gameBoard);
}

function checkDeath() {
    gameOver = outsideGrid(getSnakeHead()) || snakeInterserction()
}